# README #

## Compile game backend
``` bash
./setup_script.sh [path_to_backend]
```

## Install requirements
``` bash
pip install -r requirements.txt
```

## Run game
``` bash
cd src
python game.py
```

## Run experiments
### Generate maps
``` bash
mkdir -p maps/112diff
```
### Login to Wandb
``` bash
wandb login
```
### CMA-ES
``` bash
cd src
python cma_es.py 1/2/3/4
```
### RL
``` bash
cd src
python rl_agent.py 1/2/3/4
```
(1/2/3/4 - presets with specific parameters)
