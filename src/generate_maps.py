from moving_block_logic import generate_boards, MovingBlockAll
import os
import numpy as np
from consts import SCALE

SEED = 2222
VERBOSE = False

# width: usize,
# height: usize,
# quantity: u8,
# start_id: u16,
# moves_std: (f32, f32),
# blank_probability: f64,
# folder_path: Option<&str>,
# seed: Option<u64>,


def generate(folder="../maps/112diff"):
    for i, size in enumerate((5, 10, 15, 25, 50, 100)):
        generate_boards(
            size,
            size,
            5 * SCALE,
            20 * i * SCALE,
            (float(size), size / 10.0),
            0.25,
            folder,
            SEED,
            VERBOSE,
        )
        generate_boards(
            size,
            size,
            10 * SCALE,
            (20 * i + 5) * SCALE,
            (float(size), size / 10.0),
            0.5,
            folder,
            SEED,
            VERBOSE,
        )
        generate_boards(
            size,
            size,
            5 * SCALE,
            (20 * i + 15) * SCALE,
            (float(size), size / 10.0),
            0.7,
            folder,
            SEED,
            VERBOSE,
        )

    # folder = "../maps/112const"
    # SIZE = 75
    # for i, blank in enumerate((0.2, 0.4, 0.5, 0.6, 0.8)):
    #     generate_boards(
    #         SIZE, SIZE, 24, 24 * i, (75.0, 10.0), blank, folder, SEED, VERBOSE
    #     )


def print_stats(t, n):
    print(n)
    print(
        np.mean(t),
        np.std(t),
        np.median(t),
        np.quantile(t, 0.25),
        np.quantile(t, 0.5),
        np.quantile(t, 0.75),
    )
    print(np.histogram(t))


def stats(folder: str):
    mv = []
    diff = []
    for i in range(120 * SCALE):
        if not os.path.exists(f"{folder}/map{i}.json"):
            print(i)
            break
        m = MovingBlockAll.from_file(f"{folder}/map{i}.json")
        mv.append(m.min_moves)
        diff.append(m.difficulty)
    print(mv)
    print_stats(mv, "Moves")
    print_stats(diff, "Difficulty")


if __name__ == "__main__":
    generate()
    print("Diff")
    stats("../maps/112diff")
    # print("Const")
    # stats("../maps/112const")
