from consts import MAPS_QUANTITY, MAX_STEPS, SURROUNDING, TORCH_DEVICE
from moving_block_logic import (
    MovingBlockSurrounding,
    MovingBlockAll,
    MovingBlockDirected,
)
from typing import Callable, List, Tuple
from torch import tensor, argmax, FloatTensor, no_grad, IntTensor
import numpy as np

SELECT_MODEL = {
    "A": MovingBlockAll,
    "S": MovingBlockSurrounding,
    "D": MovingBlockDirected,
    "2": MovingBlockDirected,
}
END_OPT = {}


def score2(
    model: Callable,
    map_folder: str,
    qt: int = MAPS_QUANTITY,
    env: str = "2",
    batch: int = 1024,
) -> Tuple[List[int], float]:
    assert env in SELECT_MODEL.keys()
    ENV = SELECT_MODEL[env]
    _flatten = _flatten_2
    results = []
    weights = []
    for map in range(0, qt, batch):
        env = [
            ENV.from_file(f"{map_folder}/map{map+i}.json", SURROUNDING)
            for i in range(batch)
            if map + i < qt
        ]
        state = [_flatten(*e.reset(None)) for e in env]
        i, end = 0, [False] * len(env)
        while not all(end):
            with no_grad():
                action = model(
                    (
                        tensor([s[0] for s in state], device=TORCH_DEVICE),
                        tensor([s[1] for s in state], device=TORCH_DEVICE),
                    )
                )
            not_end_env = [_e for _e, f in zip(env, end) if not f]
            res = [
                e.step(int(a) + 1) for e, a in zip(not_end_env, action[1].cpu().numpy())
            ]  # (b, d), r, end, _
            state = [_flatten(*s) for s, _, e, _ in res if not e]
            i += 1
            for ((_, r, e, _), (ix, fin)) in zip(
                res, [_i for _i in enumerate(end) if not _i[1]]
            ):
                if e:
                    results.append(r)
                    end[ix] = True
        weights += [e.difficulty for e in env]
    # print("")
    return results, float(sum([r * w for r, w in zip(results, weights)])) / sum(weights)


def score(
    model: Callable,
    map_folder: str,
    qt: int = MAPS_QUANTITY,
    env: str = "D",
    batch: int = 1024,
) -> Tuple[List[int], float]:
    assert env in SELECT_MODEL.keys()
    ENV = SELECT_MODEL[env]
    if env == "2":
        return score2(model, map_folder, qt, env)
    elif env == "D":
        _flatten = _flatten_D
    else:
        _flatten = _flatten_S
    results = []
    weights = []
    for map in range(0, qt, batch):
        env = [
            ENV.from_file(f"{map_folder}/map{map+i}.json", SURROUNDING)
            for i in range(batch)
            if map + i < qt
        ]
        state = [_flatten(*e.reset(None)) for e in env]
        i, end = 0, [False] * len(env)
        while not all(end):
            # print(end, len(results))
            # print(state)
            with no_grad():
                action = model(tensor(state, device=TORCH_DEVICE))
            # print(
            #     int(argmax(action[0].probs)) + 1, action[1]
            # )  # argmax(action[0].probs)
            # print(action[1])
            not_end_env = [_e for _e, f in zip(env, end) if not f]
            res = [
                e.step(int(a) + 1) for e, a in zip(not_end_env, action[1].cpu().numpy())
            ]  # (b, d), r, end, _
            state = [_flatten(*s) for s, _, e, _ in res if not e]
            i += 1
            for ((_, r, e, _), (ix, fin)) in zip(
                res, [_i for _i in enumerate(end) if not _i[1]]
            ):
                if e:
                    # if r <= 0:
                    # results.append(r + i)
                    # else:
                    # elif r <= 500:
                    #     results.append()
                    # else:
                    #     results.append(MAX_STEPS - i + 11)
                    results.append(r)
                    end[ix] = True
        weights += [e.difficulty for e in env]
    # sc = float(sum([r * w for r, w in zip(results, weights)])) / sum(weights)
    # print(np.mean(results), np.std(results), np.average(results, weights=weights))
    return results, np.average(results, weights=weights)


def _flatten_2(board, dist):
    return board, dist


def _flatten_D(board, dist):
    b = []
    for v in board:
        b += v
    b += dist
    return b


def _flatten_S(board, dist):
    b = []
    for v in board:
        b += v
    b += [dist]
    return b


if __name__ == "__main__":
    import gym
    from pl_bolts.models.rl.ppo_model import PPO
    from pprint import pprint

    gym.envs.register(
        id="moving_block_directed",
        entry_point="gym_enviroment:MovingBlockDirectedEnv",
        kwargs={"maps_folder": "../maps/112diff", "surrounding": SURROUNDING},
    )

    ppo_params = {
        "batch_size": 256,
        "max_episode_len": 120,
        "steps_per_epoch": 2048,
    }
    model = PPO(env="moving_block_directed", **ppo_params)
    r, w = score(model, "../maps/112diff", qt=MAPS_QUANTITY)
    print(r)
    print(w)
