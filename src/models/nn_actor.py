import torch
from torch import device, nn
from torch.distributions.categorical import Categorical
from torchsummary import summary
from consts import TORCH_DEVICE as TD


class NNActorCritic(nn.Module):
    def __init__(self, input_shape, output_shape, hidden_size=128, emb_size=3):
        super().__init__()
        self.emb = nn.Sequential(nn.Embedding(6, emb_size, device=TD), nn.Flatten())
        self.net = nn.Sequential(
            nn.Linear(emb_size * (input_shape - 2) + 2, hidden_size, device=TD),
            nn.ReLU(),
            nn.Linear(hidden_size, output_shape, device=TD),
        )

    def forward(self, input):
        # print("<<<<<<<>>>>>>>>")
        # print(input[:, :-2])
        embed = self.emb(input[:, :-2].int())
        # print(input[:, -2:])
        return self.net(torch.concat((embed, input[:, -2:]), dim=-1))

    def __str__(self) -> str:
        return "NN"


class NNActor(NNActorCritic):
    def __init__(self, input_shape, output_shape, hidden_size=128, emb_size=3):
        super().__init__(input_shape, output_shape, hidden_size, emb_size)

        summary(self, (input_shape,))

    def forward(self, input):
        logits = super().forward(input)
        c = Categorical(logits=logits)
        return (
            c,
            torch.argmax(c.logits, dim=-1),
        )
