import numpy as np
from torch import nn
from torch.distributions.categorical import Categorical
from torchsummary import summary
import torch

from consts import TORCH_DEVICE as TD


class CNNActorCritic(nn.Module):
    def __init__(
        self,
        input_shape,
        output_shape,
        hidden_size=64,
        emb_size=3,
        filters=6,
        filters_out=3,
    ) -> None:
        super(CNNActorCritic, self).__init__()  # nn.Module, self
        self.emb = nn.Embedding(6, emb_size, device=TD)
        self.reshape_shape = (-1, emb_size, *input_shape)
        self.conv = nn.Sequential(
            nn.Conv2d(emb_size, filters, kernel_size=3, device=TD),
            nn.ReLU(),
            nn.Conv2d(filters, filters_out, kernel_size=3, device=TD),
            nn.ReLU(),
            nn.Flatten(),
        )

        conv_shape = self._get_conv_out_shape(input_shape)
        self.deep = nn.Sequential(
            nn.Linear(conv_shape + 2, hidden_size, device=TD),
            nn.ReLU(),
            nn.Linear(hidden_size, output_shape, device=TD),
        )

    def _get_conv_out_shape(self, shape):
        conv_out = self.conv(
            torch.zeros(1, *((self.reshape_shape[1],) + shape), device=TD)
        )
        # print("@@@@@@@@@", conv_out.size()[1])
        return conv_out.size()[1]

    def forward(self, input):
        # print("@@@@@@@@@@@@", len(input), len(input[0]), len(input[1]))
        input_conv = self.emb(input[0].int()).view(*self.reshape_shape)
        # print("!!!!!!!!!!", input_t.size())
        # print("@@@@@@@@@@@@", input_t)
        af_conv = self.conv(input_conv)
        # print("!!!!!!!!!!", af_conv.size(), input[1].size())
        to_deep = torch.concat([af_conv, input[1]], dim=1)
        return self.deep(to_deep)


class CNNActor(CNNActorCritic):
    def __init__(
        self,
        input_shape,
        output_shape,
        hidden_size=64,
        emb_size=3,
        filters=6,
        filters_out=3,
    ) -> None:
        super().__init__(
            input_shape, output_shape, hidden_size, emb_size, filters, filters_out
        )

    def forward(self, input):
        logits = super().forward(input)
        c = Categorical(logits=logits)
        return c, torch.argmax(logits, dim=-1)

    def __str__(self) -> str:
        return "CNN"
