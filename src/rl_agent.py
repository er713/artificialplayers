from statistics import mode
import sys
import gym
import torch
from pl_bolts.models.rl.ppo_model import PPO
from ppo2d import PPO2D
from ppo_emb import PPOEmb
from pytorch_lightning import Trainer, seed_everything
from pytorch_lightning.loggers import WandbLogger

from score_model import score
from consts import MAPS_QUANTITY, MAX_STEPS, SURROUNDING, DEVICE


def start_rl(
    nn_type,
    epochs=600,
    seed=2137,
    lr_actor=3.0e-4,
    lr_critic=1.0e-3,
    batch_size=1536,
    ste=6,
):
    gym.envs.register(
        id="moving_block_all",
        entry_point="gym_enviroment:MovingBlockAllEnv",
        kwargs={"maps_folder": "../maps/112const"},
    )
    gym.envs.register(
        id="moving_block_surrounding",
        entry_point="gym_enviroment:MovingBlockSurroundingEnv",
        kwargs={"maps_folder": "../maps/112diff", "surrounding": SURROUNDING},
    )
    gym.envs.register(
        id="moving_block_directed",
        entry_point="gym_enviroment:MovingBlockDirectedEnv",
        kwargs={"maps_folder": "../maps/112diff", "surrounding": SURROUNDING},
    )
    gym.envs.register(
        id="moving_block_directed_2d",
        entry_point="gym_enviroment:MovingBlock2DEnv",
        kwargs={"maps_folder": "../maps/112diff", "surrounding": SURROUNDING},
    )
    seed_everything(seed)

    ppo_params = {
        "batch_size": batch_size,
        "max_episode_len": MAX_STEPS,
        "steps_per_epoch": batch_size * ste,
        "lr_actor": lr_actor,
        "lr_critic": lr_critic,
    }
    if nn_type == PPOEmb:
        model = PPOEmb(env="moving_block_directed", **ppo_params)
    elif nn_type == PPO2D:
        model = PPO2D(env="moving_block_directed_2d", **ppo_params)
    elif nn_type == PPO:
        model = PPO(env="moving_block_directed", **ppo_params)

    wandb_logger = WandbLogger(
        name=f"RL_{'MLP' if nn_type==PPO else str(model)}", project="SIwG"
    )
    wandb_logger.log_hyperparams(
        {
            "nn_type": "MLP" if nn_type == PPO else str(model),
            "seed": seed,
            "epochs": epochs,
            **ppo_params,
        }
    )
    trainer = Trainer(
        deterministic=False,
        max_epochs=epochs,
        gpus=1 if DEVICE.startswith("cuda") else None,
        logger=wandb_logger,
    )
    trainer.fit(model)

    # model.load_from_checkpoint(
    #     "./lightning_logs/version_100/checkpoints/epoch=499-step=4000.ckpt"
    # )

    r, b = score(
        model,
        "../maps/112diff",
        env="D" if nn_type != PPO2D else "2",
        qt=MAPS_QUANTITY,
    )
    # print(r)
    print(b)
    wandb_logger.log_metrics({"end_fitness": b, "map_rewards": r})


if __name__ == "__main__":
    arg = int(sys.argv[1])
    if arg == 1:
        start_rl(
            PPO, epochs=4000, batch_size=1024, ste=4, lr_actor=9.0e-4, lr_critic=1.0e-4
        )
    elif arg == 2:
        start_rl(
            PPO,
            epochs=4000,
            batch_size=1024,
            ste=4,
            lr_actor=9.0e-4,
            lr_critic=1.0e-4,
            seed=2139,
        )
    # start_rl(
    #    PPOEmb,
    #   epochs=200,
    #  batch_size=1024,
    #  ste=4,
    #  lr_actor=9.0e-4,
    #  lr_critic=1.5e-2,
    # )
    elif arg == 3:
        start_rl(
            PPO,
            epochs=200,
            batch_size=1024,
            ste=4,
            lr_actor=9.0e-4,
            lr_critic=1.0e-4,
            seed=2139,
        )
        # start_rl(PPO2D)
    elif arg == 4:
        start_rl(
            PPO, epochs=200, batch_size=1024, ste=4, lr_actor=9.0e-4, lr_critic=1.0e-4
        )
        start_rl(PPO2D)
