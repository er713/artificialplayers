from __future__ import unicode_literals
import random
from moving_block_logic import MovingBlockDirected
from generate_maps import generate
from consts import SURROUNDING
import os

from curtsies import FullscreenWindow, Input, FSArray
from curtsies.fmtfuncs import (
    red,
    bold,
    green,
    on_blue,
    yellow,
    on_dark,
    on_yellow,
    on_red,
    on_black,
    on_green,
    on_cyan,
    underline,
    on_magenta,
)

B2S = {
    0: on_black(" "),
    1: on_cyan(" "),
    5: on_yellow(" "),
    2: on_red(" "),
    3: on_green(" "),
    4: on_magenta(" "),
}

MG = 4
MAPS_FOLDER = "../maps/game/"
LOOP = True


def state_to_window(tab, board, v):
    for l, line in enumerate(board):
        for r, emb in enumerate(line):
            tab[MG + l, MG + r] = [B2S[emb]]
    goal = (
        bold("Goal: ")
        + f"{abs(v[0])} {'DOWN' if v[0] > 0 else 'UP'} {abs(v[1])} {'RIGHT' if v[1] > 0 else 'LEFT'}"
    )
    tab[MG + len(board) + 1, MG : MG + len(goal)] = [goal]
    tab[MG + len(board) + 1, MG + len(goal) :] = [" " * (tab.width - MG - len(goal))]


if not os.path.isfile(MAPS_FOLDER + "map0.json") or not os.path.isfile(
    MAPS_FOLDER + "map2399.json"
):
    os.makedirs(MAPS_FOLDER, exist_ok=True)
    generate(MAPS_FOLDER)

while LOOP:
    with FullscreenWindow() as screen:
        tab = FSArray(screen.height, screen.width)
        msg1 = bold("Choose map [0-2399]: ")
        msg2 = "Press ESC or q to quit"
        num = ""
        curs = MG + len(msg1)
        tab[MG, MG:curs] = [msg1]
        tab[MG, curs] = [underline(" ")]
        tab[MG + 2, MG : MG + len(msg2)] = [msg2]
        screen.render_to_terminal(tab)

        with Input() as input_gen:
            for c in input_gen:
                if "0" <= c <= "9":
                    if len(num) < 4:
                        num += c
                elif c == "<BACKSPACE>":
                    num = num[:-1]
                elif c == "<Ctrl-j>":
                    if len(num) > 0:
                        choosen = int(str(num))
                        if 0 <= choosen <= 2399:
                            break
                elif c == "<ESC>" or c == "q":
                    LOOP = False
                    break
                tab[MG, curs : curs + 5] = [num + underline(" ") + " " * (4 - len(num))]
                screen.render_to_terminal(tab)
        tab[MG, MG : curs + 5] = [" " * (curs + 5 - MG)]
        tab[MG + 2, MG : MG + len(msg2)] = [" " * len(msg2)]

        if not LOOP:
            break

        env = MovingBlockDirected.from_file(
            MAPS_FOLDER + f"map{choosen}.json", SURROUNDING
        )
        (board, v) = env.reset(None)
        end, r = False, 0
        with Input() as game:
            state_to_window(tab, board, v)
            screen.render_to_terminal(tab)
            for a in game:
                if end:
                    break
                if a == "<UP>":
                    (board, v), r, end, _ = env.step(1)
                elif a == "<RIGHT>":
                    (board, v), r, end, _ = env.step(2)
                elif a == "<DOWN>":
                    (board, v), r, end, _ = env.step(3)
                elif a == "<LEFT>":
                    (board, v), r, end, _ = env.step(4)
                elif a == "<ESC>" or a == "q":
                    LOOP = False
                    break
                state_to_window(tab, board, v)
                screen.render_to_terminal(tab)

        if not LOOP:
            break

        for l, line in enumerate(board):
            for _r, emb in enumerate(line):
                tab[MG + l, MG + _r] = [" "]
        tab[MG + 2, MG : MG + len(msg2)] = [msg2]
        msg3 = "Other key to RESTART"
        tab[MG + 3, MG : MG + len(msg3)] = [msg3]
        if r >= 1000:
            result = "You win!!!"
            tab[MG, MG : MG + len(result)] = [result]
        else:
            result = "You lose :("
            tab[MG, MG : MG + len(result)] = [result]
        with Input() as end_game:
            screen.render_to_terminal(tab)
            for c in end_game:
                if c == "<ESC>" or c == "q":
                    LOOP = False
                    break
                else:
                    tab[MG, MG : MG + len(result)] = [" " * len(result)]
                    tab[MG + 2, MG : MG + len(msg2)] = [" " * len(msg2)]
                    tab[MG + 3, MG : MG + len(msg3)] = [" " * len(msg3)]
                    break
