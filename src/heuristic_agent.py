from consts import MAPS_QUANTITY, MAX_STEPS, SURROUNDING, WANDB_PROJECT_NAME
from moving_block_logic import (
    MovingBlockDirected,
)
import wandb
from typing import List, Tuple, Optional, Set
from random import choices, choice
import numpy as np
from tqdm import tqdm


UP_DOWN = {1, 3}


def choose_up_down(available: Set, down: bool, p: float = 0.9) -> Optional[int]:
    moves = UP_DOWN.intersection(available)

    move = choices([3, 1] if down else [1, 3], [p, 1 - p], k=1)[0]

    if move in moves:
        return move
    else:
        return None


RIGHT_LEFT = {2, 4}


def choose_right_left(available: Set, right: bool, p: float = 0.9) -> Optional[int]:
    moves = RIGHT_LEFT.intersection(available)

    move = choices([2, 4] if right else [4, 2], [p, 1 - p], k=1)[0]

    if move in moves:
        return move
    else:
        return None


OPPOSITE_CHOOSE = {choose_up_down: choose_right_left, choose_right_left: choose_up_down}


def score_heuristic(map_folder: str = "../maps/112diff", p: float = 0.9) -> float:
    numerator = 0
    denominator = 0

    for map in (
        MovingBlockDirected.from_file(f"{map_folder}/map{i}.json", SURROUNDING)
        for i in range(MAPS_QUANTITY)
    ):
        board, dist = map.reset(None)
        end = False

        while not end:
            board = np.array(board)
            # print(board)

            if board[SURROUNDING] == 4:  # standing piece
                # print("standing piece")
                clock_x = [(1, 2), (3, 3), (4, 5), (3, 3)]
                clock_y = [(3, 3), (4, 5), (3, 3), (1, 2)]
            else:  # laying piece
                id = np.where(np.logical_or(board == 3, board == 2))
                id = np.array(list(zip(id[0], id[1])))
                if id[0, 0] == id[1, 0]:  # horizontal
                    # print("horizontal")
                    _min_y = np.min(id[:, 1])
                    clock_x = [id[:, 0] - 1, (id[0, 0],), id[:, 0] + 1, (id[0, 0],)]
                    clock_y = [id[:, 1], (_min_y + 2,), id[:, 1], (_min_y - 1,)]
                    pass
                elif id[0, 1] == id[1, 1]:  # vertical
                    # print("vertical")
                    _min_x = np.min(id[:, 0])
                    clock_x = [(_min_x - 1,), id[:, 0], (_min_x + 2,), id[:, 0]]
                    clock_y = [(id[0, 1],), id[:, 1] + 1, (id[0, 1],), id[:, 1] - 1]
                else:
                    raise Exception("Must be horizontal or vertical")

            moves = [list(zip(a, b)) for a, b in zip(clock_x, clock_y)]
            # print(moves)
            available_moves = set()
            for i, places in enumerate(moves):
                if all((board[place] != 0 for place in places)):
                    available_moves.add(i + 1)
            # print(available_moves)

            direction = choices([0, 1], [abs(v) for v in dist], k=1)[0]

            choose = None
            if direction == 0:  # UP/DOWN
                choose = choose_up_down
            else:  # RIGHT/LEFT
                choose = choose_right_left
            move = choose(available_moves, dist[direction] > 0, p)

            if move is None:
                move = OPPOSITE_CHOOSE[choose](
                    available_moves, dist[(direction + 1) % 2] > 0, p
                )

            if move is None:
                move = choice(list(available_moves))

            if move is None:
                raise Exception("Imposible -- at leaste reversed last move")

            (board, dist), reward, end, _ = map.step(move)
            board = np.array(board)

        numerator += map.difficulty * reward
        denominator += map.difficulty
        # break

    return numerator / denominator


if __name__ == "__main__":
    for p in np.arange(0.6, 0.70001, 0.01):
        print(
            f"p={p} -- avg score:\t{np.mean([score_heuristic(p=p) for _ in tqdm(range(12))])}"
        )

    # print(score_heuristic())


# p=0.5 -- avg score:     583.3842463738237
# p=0.55 -- avg score:    621.71484262588
# p=0.6 -- avg score:     644.891439668243
# p=0.65 -- avg score:    649.2153499899064
# p=0.7 -- avg score:     635.1353330455586
# p=0.75 -- avg score:    619.7052569355858
# p=0.75 -- avg score:    616.861175001396
# p=0.8 -- avg score:     598.1124893158263
# p=0.85 -- avg score:    575.3988278548757
# p=0.9 -- avg score:     550.654191954334
# p=0.95 -- avg score:    528.9904363223249
# p=0.99 -- avg score:    508.4772668702565
