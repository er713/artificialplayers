import os
import sys
from deap import creator, base, tools, algorithms, cma
import numpy as np
import torch
import wandb
from consts import WANDB_PROJECT_NAME, TORCH_DEVICE

from models.nn_actor import NNActor
from models.cnn_ac import CNNActor
from score_model import score

NN = None
SCORE_TYPE = None


def w_score(mlp):
    i = 0
    for p in NN.parameters():
        qt = np.prod(p.size())
        p.data.copy_(torch.tensor(mlp[i : i + qt], device=TORCH_DEVICE).view(p.size()))
        i += qt
    NN.to(device=TORCH_DEVICE)
    return (score(NN, "../maps/112diff", env=SCORE_TYPE)[1],)


creator.create("FitnessM", base.Fitness, weights=(1.0,))
creator.create("MLP", list, fitness=creator.FitnessM)

toolbox = base.Toolbox()
toolbox.register("evaluate", w_score)


def start_cma_es(nn_type, sigma=7.0, hidden_size=8, ngen=50, seed=2137):
    global NN, SCORE_TYPE, strategy, generation
    if nn_type == NNActor:
        NN = NNActor(7 * 7 + 2, 4, hidden_size=hidden_size)
    else:
        NN = CNNActor((7, 7), 4, hidden_size=hidden_size, filters=4, filters_out=6)
    SCORE_TYPE = "D" if type(NN) == NNActor else "2"

    params = sum([np.prod(p.size()) for p in NN.parameters()])

    wandb.init(
        entity="er713",
        project=WANDB_PROJECT_NAME,
        name=f"CMA-ES_{str(NN)}",
        config={
            "sigma": sigma,
            "nn_type": str(NN),
            "hidden_size": hidden_size,
            "seed": seed,
            "ngen": ngen,
            "model_params": params,
        },
    )
    np.random.seed(seed)

    print("TRAINABLE PARAMS:\n", params)

    strategy = cma.Strategy(
        centroid=np.random.normal(size=params),
        sigma=sigma,
    )
    toolbox.register("generate", strategy.generate, creator.MLP)
    toolbox.register("update", update_and_log)

    hof = tools.HallOfFame(3)
    stats = tools.Statistics(lambda mlp: mlp.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    generation = 0
    algorithms.eaGenerateUpdate(toolbox, ngen=ngen, stats=stats, halloffame=hof)

    wandb.config.update(
        {"best_fitness": hof[0].fitness.values[0], "best_params": hof[0]}
    )
    wandb.finish(0)


def update_and_log(population):
    global generation, strategy
    fit = [p.fitness.values for p in population]
    wandb.log(
        {
            "avg": np.mean(fit),
            "std": np.std(fit),
            "min": np.min(fit),
            "max": np.max(fit),
            "gen": generation,
        }
    )
    generation += 1
    strategy.update(population)


if __name__ == "__main__":
    arg = int(sys.argv[1])
    if arg == 1:
        start_cma_es(NNActor, 7, 8, seed=2137)
    elif arg == 2:
        start_cma_es(NNActor, 7, 8, seed=2139)
    elif arg == 3:
        start_cma_es(CNNActor, 7, 16, seed=2137)
    elif arg == 4:
        start_cma_es(CNNActor, 7, 16, seed=2139)
