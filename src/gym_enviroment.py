from gym import Env
from gym.spaces import Discrete, Box, Tuple
from moving_block_logic import (
    MovingBlockSurrounding,
    MovingBlockAll,
    MovingBlockDirected,
)
import numpy as np
from numpy.random import randint, seed as np_seed
from abc import ABC
from typing import Optional
from consts import MAPS_QUANTITY


class MovingBlockEnv(Env, ABC):
    def __init__(self, maps_folder: str, mb_class) -> None:
        super().__init__()
        self.maps_folder = maps_folder
        self.mb_class = mb_class

        self.rust_env = None
        self.map_from_file()

        self.action_space = Discrete(n=4)
        self.reward_range = (-100, 1120)

    def step(self, action):
        # print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", action)
        (b, d), r, e, _ = self.rust_env.step(int(action) + 1)
        return self.flatten(b, d), r, e, _

    def reset(
        self,
        *,
        seed: Optional[int] = None,
        return_info: bool = False,
        options: Optional[dict] = None,
    ):
        self.rust_env.reset()
        if seed is not None:
            self.seed(seed=seed)
        self.map_from_file()
        b, d = self.rust_env.reset()
        return self.flatten(b, d)

    def render(self, mode="human"):
        pass

    def close(self):
        pass

    def seed(self, seed=None):
        if seed is not None:
            np_seed(seed)

    def map_from_file(self):
        self.map_number = randint(0, MAPS_QUANTITY)
        self.rust_env = self.mb_class.from_file(
            f"{self.maps_folder}/map{self.map_number}.json"
        )

    def flatten(self, board, dist):
        b = []
        for v in board:
            b += v
        b += [dist]
        return b


class MovingBlockAllEnv(MovingBlockEnv):
    def __init__(self, maps_folder: str) -> None:
        super().__init__(maps_folder, MovingBlockAll)
        self.observation_space = Box(low=0, high=300, shape=(79 * 79 + 1,), dtype=int)

    # def step(self, action):
    #     (board, d), r, e, _ = super().step(action)
    #     b = []
    #     for v in board:
    #         b += v
    #     b += [d]
    #     return b, r, e, _

    # def reset(self, *,
    #     seed: Optional[int] = None,
    #     return_info: bool = False,
    #     options: Optional[dict] = None,):
    #     board, d = super().reset(seed, return_info, options)


class MovingBlockSurroundingEnv(MovingBlockEnv):
    def __init__(self, maps_folder: str, surrounding) -> None:
        self.surrounding = surrounding
        super().__init__(maps_folder, MovingBlockSurrounding)
        self.observation_space = Box(
            low=np.array(
                [0] * ((2 * self.surrounding[0] + 1) * (2 * self.surrounding[1] + 1))
                + [0]
            ),
            high=np.array(
                [5] * ((2 * self.surrounding[0] + 1) * (2 * self.surrounding[1] + 1))
                + [300]
            ),
            shape=((2 * self.surrounding[0] + 1) * (2 * self.surrounding[1] + 1) + 1,),
            dtype=int,
        )

    def map_from_file(self):
        self.map_number = randint(0, MAPS_QUANTITY)
        self.rust_env = self.mb_class.from_file(
            f"{self.maps_folder}/map{self.map_number}.json", self.surrounding
        )


class MovingBlockDirectedEnv(MovingBlockEnv):
    def __init__(self, maps_folder: str, surrounding) -> None:
        self.surrounding = surrounding
        super().__init__(maps_folder, MovingBlockDirected)
        self.observation_space = Box(
            low=np.array(
                [0] * ((2 * self.surrounding[0] + 1) * (2 * self.surrounding[1] + 1))
                + [-150, -150]
            ),
            high=np.array(
                [5] * ((2 * self.surrounding[0] + 1) * (2 * self.surrounding[1] + 1))
                + [150, 150]
            ),
            shape=((2 * self.surrounding[0] + 1) * (2 * self.surrounding[1] + 1) + 2,),
            dtype=int,
        )

    def map_from_file(self):
        self.map_number = randint(0, MAPS_QUANTITY)
        self.rust_env = self.mb_class.from_file(
            f"{self.maps_folder}/map{self.map_number}.json", self.surrounding
        )

    def flatten(self, board, dist):
        b = []
        for v in board:
            b += v
        b += dist
        return b


class MovingBlock2DEnv(MovingBlockDirectedEnv):
    def __init__(self, maps_folder: str, surrounding) -> None:
        self.surrounding = surrounding
        super().__init__(maps_folder, surrounding)
        self.observation_space = Tuple(
            (
                Box(
                    low=0,
                    high=5,
                    shape=(
                        (2 * self.surrounding[0] + 1),
                        (2 * self.surrounding[1] + 1),
                    ),
                    dtype=int,
                ),
                Box(low=-120, high=120, shape=(2,), dtype=int),
            ),
        )

    def flatten(self, board, dist):
        return board, dist
