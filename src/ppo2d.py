from pl_bolts.models.rl.ppo_model import PPO
from pl_bolts.models.rl.common.networks import ActorCategorical, ActorContinous
import gym
import torch
from torch import Tensor
from typing import Any, Tuple, List
from models.cnn_ac import CNNActorCritic


class PPO2D(PPO):
    def __init__(
        self,
        env: str,
        gamma: float = 0.99,
        lam: float = 0.95,
        lr_actor: float = 3e-4,
        lr_critic: float = 1e-3,
        max_episode_len: float = 200,
        batch_size: int = 512,
        steps_per_epoch: int = 2048,
        nb_optim_iters: int = 4,
        clip_ratio: float = 0.2,
        hidden_size=128,
        **kwargs: Any,
    ) -> None:
        super().__init__(env="moving_block_directed")

        # Hyperparameters
        self.lr_actor = lr_actor
        self.lr_critic = lr_critic
        self.steps_per_epoch = steps_per_epoch
        self.nb_optim_iters = nb_optim_iters
        self.batch_size = batch_size
        self.gamma = gamma
        self.lam = lam
        self.max_episode_len = max_episode_len
        self.clip_ratio = clip_ratio
        self.save_hyperparameters()

        self.env = gym.make(env)
        # value network
        self.critic = CNNActorCritic(
            self.env.observation_space[0].shape, 1, hidden_size=hidden_size
        )
        # policy network (agent)
        if isinstance(self.env.action_space, gym.spaces.box.Box):
            act_dim = self.env.action_space.shape[0]
            actor_mlp = CNNActorCritic(
                self.env.observation_space[0].shape, act_dim, hidden_size=hidden_size
            )
            self.actor = ActorContinous(actor_mlp, act_dim)
        elif isinstance(self.env.action_space, gym.spaces.discrete.Discrete):
            actor_mlp = CNNActorCritic(
                self.env.observation_space[0].shape,
                self.env.action_space.n,
                hidden_size=hidden_size,
            )
            self.actor = ActorCategorical(actor_mlp)
        else:
            raise NotImplementedError(
                "Env action space should be of type Box (continous) or Discrete (categorical). "
                f"Got type: {type(self.env.action_space)}"
            )

        self.batch_states1 = []
        self.batch_states2 = []
        self.batch_actions = []
        self.batch_adv = []
        self.batch_qvals = []
        self.batch_logp = []

        self.ep_rewards = []
        self.ep_values = []
        self.epoch_rewards = []

        self.episode_step = 0
        self.avg_ep_reward = 0
        self.avg_ep_len = 0
        self.avg_reward = 0

        st1, st2 = self.env.reset()
        self.state = (torch.FloatTensor([st1]), torch.FloatTensor(st2))
        # print("!!!!!!!!!!!!!!", self.state)

    def generate_trajectory_samples(
        self,
    ) -> Tuple[List[Any], List[Any], List[Any]]:
        """Contains the logic for generating trajectory data to train policy and value network.

        Yield:
           Tuple of Lists containing tensors for states, actions, log probs, qvals and advantage
        """

        for step in range(self.steps_per_epoch):
            # print("@@@@@", self.state[0])
            self.state = (
                self.state[0].to(device=self.device),
                self.state[1].to(device=self.device),
            )

            with torch.no_grad():
                pi, action, value = self(
                    (self.state[0].unsqueeze(0), self.state[1].unsqueeze(0))
                )
                log_prob = self.actor.get_log_prob(pi, action)

            next_state, reward, done, _ = self.env.step(action.cpu().numpy())

            self.episode_step += 1

            self.batch_states1.append(self.state[0])
            self.batch_states2.append(self.state[1])
            self.batch_actions.append(action)
            self.batch_logp.append(log_prob)

            self.ep_rewards.append(reward)
            self.ep_values.append(value.item())

            self.state = (
                torch.FloatTensor([next_state[0]]),
                torch.FloatTensor(next_state[1]),
            )

            epoch_end = step == (self.steps_per_epoch - 1)
            terminal = len(self.ep_rewards) == self.max_episode_len

            if epoch_end or done or terminal:
                # if trajectory ends abtruptly, boostrap value of next state
                if (terminal or epoch_end) and not done:
                    self.state = (
                        self.state[0].to(device=self.device),
                        self.state[1].to(device=self.device),
                    )
                    with torch.no_grad():
                        _, _, value = self(
                            (self.state[0].unsqueeze(0), self.state[1].unsqueeze(0))
                        )
                        last_value = value.item()
                        steps_before_cutoff = self.episode_step
                else:
                    last_value = 0
                    steps_before_cutoff = 0

                # discounted cumulative reward
                self.batch_qvals += self.discount_rewards(
                    self.ep_rewards + [last_value], self.gamma
                )[:-1]
                # advantage
                self.batch_adv += self.calc_advantage(
                    self.ep_rewards, self.ep_values, last_value
                )
                # logs
                self.epoch_rewards.append(sum(self.ep_rewards))
                # reset params
                self.ep_rewards = []
                self.ep_values = []
                self.episode_step = 0
                st1, st2 = self.env.reset()
                self.state = (torch.FloatTensor([st1]), torch.FloatTensor(st2))

            if epoch_end:
                train_data = zip(
                    self.batch_states1,
                    self.batch_states2,
                    self.batch_actions,
                    self.batch_logp,
                    self.batch_qvals,
                    self.batch_adv,
                )

                for state1, state2, action, logp_old, qval, adv in train_data:
                    yield (state1, state2), action, logp_old, qval, adv

                self.batch_states1.clear()
                self.batch_states2.clear()
                self.batch_actions.clear()
                self.batch_adv.clear()
                self.batch_logp.clear()
                self.batch_qvals.clear()

                # logging
                self.avg_reward = sum(self.epoch_rewards) / self.steps_per_epoch

                # if epoch ended abruptly, exlude last cut-short episode to prevent stats skewness
                epoch_rewards = self.epoch_rewards
                if not done:
                    epoch_rewards = epoch_rewards[:-1]

                total_epoch_reward = sum(epoch_rewards)
                nb_episodes = len(epoch_rewards)

                self.avg_ep_reward = total_epoch_reward / nb_episodes
                self.avg_ep_len = (
                    self.steps_per_epoch - steps_before_cutoff
                ) / nb_episodes

                self.epoch_rewards.clear()

    def forward(self, x: Any) -> Tuple[Any, Any, Any]:
        """Passes in a state x through the network and returns the policy and a sampled action.

        Args:
            x: environment state

        Returns:
            Tuple of policy and action
        """
        # print("<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>")
        pi, action = self.actor(x)
        value = self.critic(x)

        return pi, action, value

    def training_step(self, batch: Tuple[Any, Any], batch_idx, optimizer_idx):
        """Carries out a single update to actor and critic network from a batch of replay buffer.

        Args:
            batch: batch of replay buffer/trajectory data
            batch_idx: not used
            optimizer_idx: idx that controls optimizing actor or critic network

        Returns:
            loss
        """
        (state1, state2), action, old_logp, qval, adv = batch
        # print("1111111", state1.size())
        # print("2222222", state2.size())

        # normalize advantages
        adv = (adv - adv.mean()) / adv.std()

        self.log(
            "avg_ep_len", self.avg_ep_len, prog_bar=True, on_step=False, on_epoch=True
        )
        self.log(
            "avg_ep_reward",
            self.avg_ep_reward,
            prog_bar=True,
            on_step=False,
            on_epoch=True,
        )
        self.log(
            "avg_reward", self.avg_reward, prog_bar=True, on_step=False, on_epoch=True
        )

        if optimizer_idx == 0:
            loss_actor = self.actor_loss((state1, state2), action, old_logp, adv)
            self.log(
                "loss_actor",
                loss_actor,
                on_step=False,
                on_epoch=True,
                prog_bar=True,
                logger=True,
            )

            return loss_actor

        if optimizer_idx == 1:
            loss_critic = self.critic_loss((state1, state2), qval)
            self.log(
                "loss_critic",
                loss_critic,
                on_step=False,
                on_epoch=True,
                prog_bar=False,
                logger=True,
            )

            return loss_critic

        raise NotImplementedError(
            f"Got optimizer_idx: {optimizer_idx}. Expected only 2 optimizers from configure_optimizers. "
            "Modify optimizer logic in training_step to account for this. "
        )

    def actor_loss(self, state, action, logp_old, adv) -> Tensor:
        pi, _ = self.actor(state)
        logp = self.actor.get_log_prob(pi, action)
        ratio = torch.exp(logp - logp_old)
        clip_adv = torch.clamp(ratio, 1 - self.clip_ratio, 1 + self.clip_ratio) * adv
        loss_actor = -(torch.min(ratio * adv, clip_adv)).mean()
        return loss_actor

    def critic_loss(self, state, qval) -> Tensor:
        value = self.critic(state)
        loss_critic = (qval - value).pow(2).mean()
        return loss_critic

    def __str__(self) -> str:
        return "CNN"
