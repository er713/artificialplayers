#!/bin/bash

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "Script for setup library with game logic
First argument -- location with repository MovingBlockLogic [default: ../movingblocklogic]";
    exit
fi

mvb_dir=${1:-../movingblocklogic}

if [ -d $mvb_dir ]; then
    current_dir=$PWD

    cd $mvb_dir;
    if [ ! -f "./Cargo.toml" ]; then
        echo "Directory do not contain RUST project";
        exit 1
    fi
    cargo build;
    cd $current_dir && 
        cp $mvb_dir/target/debug/libmoving_block_logic.so src/moving_block_logic.so;
    echo "    Copied!";
    exit
else 
    echo "${mvb_dir} is not a directory";
    exit 1
fi
